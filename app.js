const users = 'users.json';
const users2 = 'https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/file/txt/1112323f-2d3c-435b-82e2-3d0354b59ee1.txt';
const options = {
    headers: {
        "Content-Type": "text/json;charset=UTF-8" //pour un corps de type chaine
    },
    mode: "no-cors", //ou same-origin, no-cors
    credentials: "omit", //ou omit, include
};
const catFacts = 'https://cat-fact.herokuapp.com/facts';

const consoleLisible = () => {
    console.log("-------------");
};

const promise = fetch(users)
    .then(response => response.json())
    .then(response => {
        const customers = response.customers;
        //AFFICHER LE CONTENU DU JSON
        console.log(customers);

        //AFFICHER LES UTILISATEURS
        const afficherUsers = () => {
            consoleLisible();
            console.log("Afficher tous les utilisateurs");
            //Boucle pour afficher les utilisateurs
            customers.forEach(user => {
                console.log(user);
            })
        }
        afficherUsers();

        //AFFICHER LA LISTE DES ANIMAUX (ordre alphabétique)
        const afficherAnimaux = () => {
            consoleLisible();
            console.log("Afficher la liste des animaux classés par ordre alphabétique");
            //Tableau vide pour récupérer les animaux
            let pets = [];
            //Boucler chaque user et y récupérer la liste des animaux
            customers.forEach(user => {
                pets = pets.concat(user.user_pets);
            });
            //Trier les animaux par ordre alphabétique (nom)
            pets.sort((a, b) => {
                    return a.pet_name < b.pet_name ? -1 : 1;
                })
                //Afficher la liste des animaux
                .forEach(pet => {
                    console.log(pet);
                })
        }
        afficherAnimaux();

        //AFFICHER LES UTILISATEURS QUI ONT AU MOINS 1 ANIMAL
        const UserAvecAnimal = () => {
            consoleLisible();
            console.log("Afficher les utilisateurs ayant au moins 1 animal");
            //Boucle pour afficher les utilisateurs
            customers.forEach(user => {
                //Sauter les users sans animaux
                if (user.user_pets.length === 0) {
                    return;
                }
                console.log(user);
            })
        }

        UserAvecAnimal();

        //AJOUTER UN OBJET MICKEY A TOUS LES UTILISATEURS
        //Créer l'objet Mickey
        let Mickey = new Object();
        Mickey.pet_name = "Mickey";
        Mickey.pet_type = "Souris";
        Mickey.pet_age = 0.1;
        //Boucler sur tous les utilisateurs et leur ajouter Mickey
        customers.forEach(user =>
            user.user_pets.push(Mickey)
        );
        //Afficher les utilisateurs
        afficherUsers();
        UserAvecAnimal();
        afficherAnimaux();
    })
    .catch(console.error)


//BONUS
const bonus = fetch(catFacts)
    .then(response => response.json())
    .then(response => {
        consoleLisible();
        console.log(response);

        //Afficher un catFact aléatoire au click du bouton "générer"
        document.getElementById("generate").addEventListener("click", afficherFacts = () => {
            //Fonction pour générer un entier aléatoire avec un maximum
            function getRandomInt(max) {
                return Math.floor(Math.random() * Math.floor(max));
            }
            //i devient un nombre aléatoire avec pour max la taille tu tableau response
            let i = getRandomInt(response.length);
            //Choisir un fact aléatoire en utilisant i
            let randomFact = response[i];
            //Afficher le text du fait aléatoire
            document.getElementById("catFacts").innerHTML = randomFact.text;
        })
    })

//Question : Pourquoi l'objet Mickey n'apparait pas dans le fichier users.json ?
//Remarque : pas de commit à chaque étape ... Mauvaise organisation !